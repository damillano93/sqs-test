const AWS = require('aws-sdk');
const uuidv4 = require('uuid/v4');

// configurar aws 

const queueUrl = '';
AWS.config.update({
  queueUrl,
  region: 'us-east-1',
  accessKeyId: '',
  secretAccessKey: ''
});

// Crear objeto SQS
const sqs = new AWS.SQS({apiVersion: '2012-11-05'});

let categoryData = {
    name: 'millan',
    id: '321',
    Action: 'david',
    service: 'Question'
}

let sqsData = {
    MessageAttributes: {
      "categoryName": {
        DataType: "String",
        StringValue: categoryData.name
      },
      "categoryId": {
        DataType: "String",
        StringValue: categoryData.id
      }
    },
    MessageBody: JSON.stringify(categoryData),
    MessageGroupId: "CategoryUpdate",
    MessageDeduplicationId: uuidv4(),
    QueueUrl: queueUrl
};

// Send the order data to the SQS queue
let sendSqsMessage = sqs.sendMessage(sqsData).promise();

sendSqsMessage.then((data) => {
    console.log(`CategoryUpdate SQS | SUCCESS: ${data.MessageId}`);
}).catch((err) => {
    console.log(`CategoryUpdate SQS | ERROR: ${err}`);
});