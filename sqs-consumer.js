const AWS = require('aws-sdk');
const { Consumer } = require('sqs-consumer');

// Configurar aws
AWS.config.update({
  region: 'us-east-1',
  accessKeyId: '',
  secretAccessKey: ''
});

const queueUrl = '';

// Crear  el cliente que sonume los eventos
const app = Consumer.create({
    queueUrl: queueUrl,
    handleMessage: async (message) => {
        let sqsMessage = JSON.parse(message.Body);
        console.log(message);
    },
    sqs: new AWS.SQS()
});

app.on('error', (err) => {
    console.error(err.message);
});

app.on('processing_error', (err) => {
    console.error(err.message);
});

console.log('SQS service is running');
app.start();